Title: La derivada de Debian «Lernstick» gana el premio «Swiss Open Source Education»
Slug: lernstick-award
Date: 2015-11-10 10:00
Author: Laura Arjona Reina
Translator: Adrià García-Alzórriz
Tags: Lernstick, debian, derivatives, awards
Lang: es
Status: draft

La derivada de Debian [Lernstick](FIXME_SEE_NOTE) ganó el premio "Swiss Open Source Education".

Lernstick es una derivada de Debian «en vivo» enfocada a escuelas. Se usa principalmente en Suiza, Austria y Alemania.
Lernstick ofrece un aprendizaje móvil y seguro y un entorno de trabajo que normalmente se usa en medios extraíbles (lápices USB, discos duros USB, tarjetas SD, etc.) y opcionalmente se puede instalar en un disco duro interno.

La distribución Lernstick contiene programas educativos, software multimedia, un paquete ofimático completo, juegos, programas para la edición digital de imágenes, aplicaciones para internet y tiene un espacio para información personal.

Se desarrolló por la Universidad de Ciencias Aplicadas el Noroeste de Suiza y educa.ch. Desde sus inicios ha estado en constante evolución. La última versión se publicó el 9 de octubre de 2015 y se basa en Debian 8 Jessie.

Los premios [CH Open Source Awards](http://www.ossawards.ch/) galardonan empresas, organismos públicos, comunidades open source y particulares que han sido decisivos e innovadores desarrollando o iniciando software libre. Lernstick ha ganado el premio especial «Open Source Education Award» de la edición de 2015, patrocinado por IBM.


Enlaces para información adicional:
* [Página de Lernstick en el censo de derivadas de  Debian](https://wiki.debian.org/Derivatives/Census/Lernstick)
* [Lernstick - resumen en inglés](http://www.imedias.ch/projekte/lernstick/lernstick_abstract_english.cfm)

NOTA
Dos enlaces de Lernstick
* Del censo de Derivadas: http://www.imedias.ch/projekte/lernstick/
* De los CH OSS Awards : https://lernstick.educa.ch/#
(Ambos en alemán)
