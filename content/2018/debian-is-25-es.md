Title: 25 años y sumando
Slug: debian-is-25
Date: 2018-08-16 8:50
Author: Ana Guerrero Lopez
Artist: Angelo Rosa
Tags: debian, birthday
Translator: Laura Arjona Reina
Lang: es
Status: published

![Debian cumple 25 años, por Angelo Rosa](|filename|/images/debian25years.png)

Cuando Ian Murdock anunció hace 25 años en comp.os.linux.development
*«la inminente terminación de una publicación de Linux nueva, [...] la Publicación Debian Linux»*,
nadie habría esperado que la «Publicación Debian Linux» llegara a ser lo que se conoce hoy día
como el Proyecto Debian, uno de los proyectos de software libre más grandes e influyentes.
Su principal producto es Debian, un sistema operativo (SO) libre para su ordenador, así como
para multitud de otros sistemas que mejoran nuestra vida. Desde el funcionamiento interno de tu
aeropuerto más cercano al sistema de entretenimiento del coche, y desde servidores de alojamiento
en la nube a los dispositivos de internet de las cosas que se comunican con ellos, Debian puede
hacerlos funcionar a todos.

Hoy, el proyecto Debian es una organización grande y floreciente con incontables equipos autoorganizados
compuestos de voluntarios. Aunque a menudo parece caótico desde fuera, el proyecto se sostiene
gracias a sus dos documentos fundacionales principales: el [Contrato Social de Debian], que proporciona
una visión de mejora de la sociedad, y las [Directrices de Software Libre de Debian], que proporcionan
una indicación de qué software se considera utilizable. Se complementan con la [Constitución] del proyecto,
que establece la estructura organizativa, y el [Código de Conducta], que establece el tono de la interacciones
en el proyecto.

Cada día de estos últimos 25 años la gente ha enviado informes de fallo y correcciones, subido paquetes,
actualizado traducciones, creado arte gráfico, organizado eventos sobre Debian, actualizado el sitio web,
enseñado a otros cómo usar Debian, y creado cientos de distribuciones derivadas.

**Aquí comenzamos otros 25 años - ¡y ojalá que muchos, muchos más!**

[Contrato Social de Debian]: https://www.debian.org/social_contract
[Directrices de Software Libre de Debian]: https://www.debian.org/social_contract#guidelines
[Constitución]: https://www.debian.org/devel/constitution
[Código de Conducta]: https://www.debian.org/code_of_conduct
