Title: DebConf19 Cheese and Wine Party
Slug: debconf19-cheese-and-wine-party
Date: 2019-07-02 14:30
Author: Adriana Cássia da Costa
Tags: debconf19, debconf, cheese, wine, party
Status: published

In less than one month we will be in Curitiba to start [DebCamp][debcamp] and
[DebConf19][debconf19] \o/

This C&W is the 15th official DebConf Cheese and Wine party. The first C&W was
improvised in Helsinki during DebConf 5, in the so-called "French" room. Cheese
and Wine parties are now a tradition for DebConf.

The event is very simple: bring good edible stuff from your country. We like
cheese and wine, but we love the surprising stuff that people bring from all
around the world or regions of Brazil. So, you can bring non-alcoholic drinks
or a typical food that you would like to share as well. Even if you don't
bring anything, feel free to participate: our priorities are our attendants
and free cheese.

We have to organize for a great party. An important part is planning - We want
to know what you are bringing, in order to prepare the labels and organizing
other things.

So, please go to our [wiki page][wikipage] and add what you will bring!

If you don't have time to buy before travel, we list some places where you can
buy cheese and wine in Curitiba. There are more information about C&W, what
you can bring, vegan cheese, Brazil customs regulations and non-alcoholic
drinks at [our site][cheese].

C&W will happen on July 22nd, 2019 (Monday) after 19h30min.

We are looking forward to seeing you all here!

![DebConf19 logo](|filename|/images/800px-Debconf19-horizontal.png)

[debcamp]: https://wiki.debian.org/DebConf/19/DebCamp
[debconf19]: https://debconf19.debconf.org
[wikipage]: https://wiki.debian.org/DebConf/19/CheeseWine#What_will_be_available_during_the_party.3F
[cheese]: https://debconf19.debconf.org/about/cheese-and-wine-party
