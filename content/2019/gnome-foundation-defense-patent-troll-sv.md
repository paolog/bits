Title: Debianprojektet står bakom GNOME-stiftelsen i försvaret mot patenttroll
Slug: gnome-foundation-defense-patent-troll
Date: 2019-10-23 10:00
Author: Ana Guerrero López
Tags: debian, gnome, patent trolls, fundraising
Translator: Andreas Rönnquist
Lang: sv
Status: published

2012 publicerade Debianprojektet vår [ståndpunkt rörande mjukvarupatent],
som anger hotet som patent utgör på fri mjukvara.

GNOME-stiftelsen har tidigare tillkännagivit att dom kämpar mot en
stämning som hävdar att Shotwell, en fotohanterare baserad på fri mjukvara,
antas kränka ett patent.

Debianprojektet står fast tillsammans med GNOME-stiftelsen i deras arbete
att visa världen att Fri mjukvarugemenskaperna kommer att försvara sig
kraftfullt mot missbruk av patentsystemet.

Vänligen läs dennna [blogpost om GNOMEs försvar mot detta patenttroll]
och överväg att göra en donation till [GNOMEs försvarsfond mot patenttroll].

[ståndpunkt rörande mjukvarupatent]: https://www.debian.org/legal/patent
[blogpost om GNOMEs försvar mot detta patenttroll]:  https://www.gnome.org/news/2019/10/gnome-files-defense-against-patent-troll/
[GNOMEs försvarsfond mot patenttroll]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
