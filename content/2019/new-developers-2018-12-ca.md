Title: Nous desenvolupadors i mantenidors de Debian (novembre i desembre del 2018)
Slug: new-developers-2018-12
Date: 2019-01-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developersen els darrers dos mesos:

  * Abhijith PA (abhijith)
  * Philippe Thierry (philou)
  * Kai-Chung Yan (seamlik)
  * Simon Quigley (tsimonq2)
  * Daniele Tricoli (eriol)
  * Molly de Blanc (mollydb)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Nicolas Mora
  * Wolfgang Silbermayr
  * Marcos Fouces
  * kpcyrd
  * Scott Martin Leggett

Enhorabona a tots!

