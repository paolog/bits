Title: A DebConf19 dá boas-vindas aos seus patrocinadores!
Slug: debconf19-welcomes-sponsors
Date: 2019-06-16 11:00
Author: znoteer and Laura Arjona Reina
Tags: debconf19, debconf, sponsors
Lang: pt-BR
Translator: Daniel Pimentel
Status: published

![DebConf19 logo](|filename|/images/800px-Debconf19-horizontal.png)

A DebConf19 acontecerá em Curitiba, Brasil, de 21 à 28 de julho de 2019.
Essa é a vigésima edição da Conferência Debian e os(as) organizadores(as) estão
trabalhando bastante para criar outro evento interessante e frutífero para os(as)
participantes.

Gostaríamos de saudar calorosamente os primeiros 29 patrocinadores da
DebConf19 e apresentá-los a vocês.

Até o momento temos três patrocinadores Platinum.

Nosso primeiro patrocinador Platinum é a [**Infomaniak**](https://www.infomaniak.com).
A Infomaniak é a maior empresa de hospedagem web na Suíça, oferecendo também
serviços de backup e armazenamento, soluções para organizadores de eventos,
transmissão ao vivo e serviços de vídeo sob demanda. Com datacenters próprios e
todos os elementos essências para o funcionamento dos serviços e produtos
fornecidos pela empresa (software e hardware).

Em seguida, como patrocinador Platinum, está o [**Google**](https://google.com).
O Google é uma das maiores empresas de tecnologia do mundo, provendo uma ampla
gama de serviços e produtos relacionados à internet como tecnologias de
publicidade online, pesquisa, computação em nuvem, software e hardware.
O Google tem apoiado o Debian através do patrocínio da DebConf há mais de
dez anos e também é um [parceiro do Debian](https://www.debian.org/partners/).

A [**Lenovo**](https://www.lenovo.com) é o nosso terceiro patrocinador Platinum.
A Lenovo é a líder global de tecnologia que fabrica um amplo portfólio de
produtos conectados, incluindo smartphones, tablets, PCs e workstations, bem como
dispositivos AR/VR, soluções inteligentes para casa/escritório e datacenter.
Este é o primeiro ano deles patrocinando a DebConf.

Nosso patrocinador Gold é a [**Collabora**](https://www.collabora.com/),
uma consultoria global que entrega soluções Open Source para o mundo commercial.
Sua expertise abrange todas as áreas chaves do desenvolvimento de software
Open Source. Além de oferecer soluções para os clientes, os engenheiros e
desenvolvedores da Collabora contribuem ativamente com muitos projetos
Open Source.

Nossos patrocinadores Prata Silver são:
[**credativ**](http://www.credativ.de/)
(uma empresa orientada à serviços com foco em software Open Source e também
um [parceiro no desenvolvimento do Debian](https://www.debian.org/partners/)),
[**Cumulus Networks**](https://cumulusnetworks.com/),
(uma empresa que constroi redes em escala web usando tecnologia de rede aberta
e inovadora),
[**Codethink**](https://www.codethink.co.uk/)
(especialistas em infraestrutura de software de baixo nível e suporte à
aplicações técnica avançadas),
[**Bern University of Applied Sciences**](https://www.bfh.ch/)
(com mais de [6.800](https://www.bfh.ch/en/bfh/facts_figures.html) estudantes
matriculados, localizada na capital Suíça),
[**Civil Infrastructure Platform**](https://www.cip-project.org/),
(um projeto colaborativo hospedado pela Linux Foundation, estabelecendo uma
base Open Source chamada “base layer” para o setor industrial),
[**\WIT**](https://www.wit.com/)
(oferecendo um solução segura em nuvem (cloud) e com completa
privacidade de dados via virtualização de hardware criptografada usando
a tecnologia Kubnernetes),
[**Hudson-Trading**](http://www.hudson-trading.com/),
(uma empresa de pesquisa e desenvolvimento de algoritmos de negociação
automatizados usando técnicas matemáticas avançadas),
[**Ubuntu**](https://www.canonical.com/),
(o Sistema Operacional desenvolvido pela Canonical),
[**NHS**](https://nhs.com.br/)
(com um amplo portfólio de produtos, eles oferecem soluções, entre outros,
para datacentres, telecomunicações, CCTV e automação residential,
commercial e industrial),
[**rentcars.com**](https://www.rentcars.com/)
que ajuda os clientes a encontrar as melhores locadoras de carros de mais de
100 locadoras em destinos nas Américas e ao redor do mundo,
e [**Roche**](https://code4life.roche.com/),
uma importante fornecedora farmacêutica internacional e empresa de pesquisa
dedicaca a cuidados de saúde personalizados.

Patrocinadores Bronze:
[**4Linux**](https://www.4linux.com.br),
[**IBM**](https://www.ibm.com/),
[**zpe**](https://www.zpesystems.com),
[**Univention**](https://www.univention.com/),
[**Policorp**](https://www.policorp.com.br),
[**Freexian**](https://www.freexian.com/services/debian-lts.html),
[**globo.com**](https://www.globo.com/).


E finalmente, nossos patrocinadores Supporter:
[**Altus Metrum**](https://altusmetrum.org/),
[**Pengwin**](https://www.pengwin.dev/),
[**ISG.EE**](https://isg.ee.ethz.ch/),
[**Jupter**](https://jupter.co/),
[**novatec**](https://novatec.com.br/),
[**Intnet**](https://intnet.com.br/),
[**Linux Professional Institute**](https://www.lpi.org/).

Obrigado a todos os nossos patrocinadores pelo apoio!
Suas contribuições fazem possível que um grande número de contribuidores(as)
Debian de todo o mundo trabalhem juntos(as), ajudem e aprendam uns com os(as)
outros(as) na DebConf19.

## Torne-se um patrocinador também!

A DebConf19 está aceitando patrocinadores.
Empresas e organizações interessadas podem entrar em contato com a
equipe da DebConf através do email
[sponsors@debconf.org](mailto:sponsors@debconf.org), e visitar o site da
DebConf19 em [https://debconf19.debconf.org](https://debconf19.debconf.org).
