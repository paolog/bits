Title: Candidates for the Debian Project Leader Elections 2013
Date: 2013-03-23 15:00
Tags: dpl, vote
Slug: candidates-for-dpl-2013
Author: Ana Guerrero
Status: published


It's the time of the year when Debian Developers vote for a new Debian Project Leader (DPL). After 3 years as DPL, Stefano Zacchiroli is not running again but we have 3 brave candidates to be his successor:

* [Gergely Nagy](https://www.debian.org/vote/2013/platforms/algernon)
* [Moray Allan](https://www.debian.org/vote/2013/platforms/moray)
* [Lucas Nussbaum](https://www.debian.org/vote/2013/platforms/lucas)

You can find more information about the candidates by following the link on their names to read their platforms, or in [the ongoing discussion in the debian-vote mailing list](https://lists.debian.org/debian-vote/2013/03/threads.html).

The campaigning period ends next Saturday, March 30th. Debian Developers will vote from March 31st to April 13th.
The new term for the project leader will start on April 17th, 2013.

All the information about this vote and the final results will be published at the [Debian Project Leader Elections 2013 voting page](https://www.debian.org/vote/2013/vote_001).
