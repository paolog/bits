Title: Nuevos desarrolladores y mantenedores de Debian (enero y febrero del 2016)
Slug: new-developers-2016-02
Date: 2016-03-14 21:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

  * Otto Kekäläinen (otto)
  * Dariusz Dwornikowski (darek)
  * Daniel Stender (stender)
  * Afif Elghraoui (afif)
  * Victor Seva (vseva)
  * James Cowgill (jcowgill)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

  * Giovani Augusto Ferreira
  * Ondřej Nový
  * Jason Pleau
  * Michael Robin Crusoe
  * Ferenc Wágner
  * Enrico Rossi
  * Christian Seiler
  * Daniel Echeverry
  * Ilias Tsitsimpis
  * James Clarke
  * Luca Boccassi

¡Felicidades a todos!
