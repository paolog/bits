Title: About
Slug: about

This is an official [Debian][debian] blog complementing other services
such as:

 * [Debian Press Release](https://www.debian.org/News/)
 * [Debian Project News](https://www.debian.org/News/weekly/)
 * [Debian account on identi.ca/](https://identi.ca/debian)


This blog is powered by [Pelican][pelican]. You can find the templates and
theme used on this blog at [salsa.debian.org][gitdo]. Patches, new themes
proposals and constructive suggestions are welcome!

This site is under the same license and copyright as the [Debian][debian]
website, see [Debian WWW pages license][wwwlicense].

If you want to contact us, please send an email to the [debian-publicity
mailing list][debian-publicity]. This is a publicly archived list.


[pelican]: http://getpelican.com/ "Find out about Pelican"
[debian]: https://www.debian.org "Debian - The Universal Operating System"
[gitdo]: https://salsa.debian.org/publicity-team/bits
[wwwlicense]: https://www.debian.org/license
[debian-publicity]: https://lists.debian.org/debian-publicity/
